// webpack.mix.js

let mix = require('laravel-mix');

mix.sass('assets/sass/app.scss', 'assets/dist').options({
    processCssUrls: false});