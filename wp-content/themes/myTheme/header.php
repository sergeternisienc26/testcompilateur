<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>testCompilateur</title>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/dist/app.css">
    <?php wp_head(); ?>
</head>

<body>
    <header>

        <div class="logos">
            <div class="logo">
                <div class="logLion">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo_UL.png" alt="">
                </div>
                <div class="logInsa">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo_INSA.png" alt="">
                </div>
            </div>
            <div class="logo">
                <div class="account">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/myAccount.png" alt="">
                </div>
                <div class="icones">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/iconesSociaux.png" alt="">
                </div>
            </div>
        </div>
        <h1>Trust Them</h1>

    </header>
    
    <?php wp_body_open(); ?>